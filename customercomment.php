<?php
/**
 * Created by PhpStorm.
 * User: alexi
 * Date: 12/03/2019
 * Time: 15:52
 */

require_once(_PS_MODULE_DIR_ . 'customercomment/models/comment.php');

class customercomment extends Module {

    public $tabs = array(
        array(
            'name' => 'Commentaires',
            'class_name' => 'AdminListingComments',
            'visible' => true,
            'parent_class_name' => 'AdminParentCustomer'
        )
    );

    public function __construct() {
        $this->name = 'customercomment';
        $this->description = $this->l('Module commentaires produit');
        $this->displayName = $this->l('Commentaires client');
        $this->version = '1.0.0';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function install(){

        return parent::install() &&
            $this->registerHook('displayCustomerAccount') &&
            $this->registerHook('displayHome') &&
            $this->_db_install() && Configuration::updateValue('comment_default_active', 1);
    }

    public function _db_install(){

        // Create table for comments on module install
        return Db::getInstance()->execute("CREATE TABLE "._DB_PREFIX_."comment(
            id_comment INT AUTO_INCREMENT PRIMARY KEY,
            comment TEXT,
            id_customer INT NOT NULL,
            date_add DATE NOT NULL,
            rate INT(1) NOT NULL,
            active BOOLEAN NOT NULL
        );");
    }

    function uninstall()
    {
        // Delete table comment on module uninstall
        $delCommentTable = Db::getInstance()->execute("DROP TABLE "._DB_PREFIX_."comment;");
        return parent::uninstall() && $delCommentTable && Configuration::deleteByName('comment_default_active');
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit'.$this->name)) {
            $myModuleName = strval(Tools::getValue('comment_default_active'));

            if (
                $myModuleName === false ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('comment_default_active', $myModuleName);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'switch',
                    'label' => $this->l('Publish comments by default'),
                    'name' => 'comment_default_active',
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'On',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ],
                        [
                            'id' => 'Off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ]
                    ]
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
            ]
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['comment_default_active'] = Configuration::get('comment_default_active');

        return $helper->generateForm($fieldsForm);
    }

    public function hookDisplayCustomerAccount() {

        // Get link to controller 'frontListingComments' on account page
        return $this->display(__FILE__, 'customercomment.tpl');
    }

    public function hookDisplayHome () {

        $sql = "SELECT c.firstname, c.lastname, a.comment, a.rate, a.date_add 
                FROM "._DB_PREFIX_."comment a
                INNER JOIN "._DB_PREFIX_."customer c
                ON c.id_customer = a.id_customer;
                ";
        $comments = Db::getInstance()->executeS($sql);

       $this->context->smarty->assign([
           'comments' => $comments
       ]);
       $this->context->controller->addCSS($this->_path.'views/css/slider.css');
       $this->context->controller->addJS($this->_path.'views/js/slider.js');
       $this->context->controller->registerStylesheet('swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css', array('media' => 'all', 'priority' => 10, 'server' => 'remote'));
       $this->context->controller->registerJavascript('swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.js', array('position' => 'all', 'priority' => 10, 'server' => 'remote'));
       return $this->display(__FILE__, 'displayHome.tpl');
    }
}