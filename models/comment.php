<?php
/**
 * Created by PhpStorm.
 * User: alexi
 * Date: 13/03/2019
 * Time: 09:51
 */

class comment extends ObjectModel {

    public $id_comment;
    public $comment;
    public $date_add;
    public $uid;
    public $rate;
    public $active;
    public static $definition = [
        'table' => 'comment',
        'primary' => 'id_comment',
        'fields' => [
            'id_comment' => array('type' => self::TYPE_INT),
            'comment' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'id_customer' => array('type' => self::TYPE_INT, 'required' => true),
            'rate' => array('type' => self::TYPE_INT, 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'required' => true, 'validate' => 'isBool')
        ]
    ];
}