<?php

require_once _PS_MODULE_DIR_ . 'customercomment/models/comment.php';

class AdminListingCommentsController extends  ModuleAdminController {

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = comment::$definition['table'];
        $this->identifier = comment::$definition['primary'];
        $this->className = comment::class;

        parent::__construct();

        // Display fields from DB in table
        $this->fields_list = [
            'id_comment' => [
                'title' => 'ID',
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ],
            'firstname' => [
                'title' => $this->module->l('Firstname'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ],
            'lastname' => [
                'title' => $this->module->l('Lastname'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ],
            'comment' => [
                'title' => $this->module->l('Comment'),
                'align' => 'center',
            ],
            'rate' => [
                'title' => $this->module->l('Rate'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ],
            'date_add' => [
                'title' => $this->module->l('Date'),
                'align' => 'center',
            ],
            'active' => [
                'title' => $this->module->l('Published'),
                'align' => 'center',
            ]
        ];

        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    public function renderList()
    {

        $this->_select = 'c.firstname, c.lastname';
        $this->_join = 'INNER JOIN ' . _DB_PREFIX_ . 'customer c ON c.id_customer = a.id_customer';
        return parent::renderList();
    }

    public function renderForm()
    {

        $rateValues = array();
        for($i=1; $i <= 5; $i++) {
            $rateValues[] = [
                'id' => 'rate',
                'value' => $i,
                'label' => $i
            ];
        }

        $this->fields_form = [
            'input' => [
                [
                    'type' => 'textarea',
                    'name' => 'comment',
                    'required' => true,
                    'label' => $this->module->l('Commentaire'),
                    'empty_message' => $this->module->l('Please fill the comment')
                ],
                [
                    'type' => 'radio',
                    'name' => 'rate',
                    'label' => $this->module->l('Note'),
                    'values' => $rateValues
                ],
                [
                    'type' => 'switch',
                    'label' => $this->module->l('Published'),
                    'name' => 'active',
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'active',
                            'value' => 1,
                            'label' => $this->module->l('Yes'),
                        ],
                        [
                            'id' => 'inactive',
                            'value' => 0,
                            'label' => $this->module->l('No'),
                        ]
                    ]
                ]
            ],
            'submit' => [
                'title' => $this->module->l('Save')
            ]
        ];

        return parent::renderForm();
    }
}