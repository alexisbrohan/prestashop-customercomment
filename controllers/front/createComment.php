<?php
/**
 * Created by PhpStorm.
 * User: alexi
 * Date: 13/03/2019
 * Time: 10:35
 */

class CustomerCommentCreateCommentModuleFrontController extends ModuleFrontController {

    public function initContent()
    {
        parent::initContent();

        if (Tools::isSubmit('comment_submit')) {
            $this->success[] = $this->l('Information successfully saved.');
        }

        // Comment already in Db
        $idCustomer = $this->context->customer->id;
        $comment = Db::getInstance()->executeS("SELECT id_comment FROM "._DB_PREFIX_ . "comment WHERE id_customer = '$idCustomer'");
        $comment = reset($comment);

        // Value form
        $commentForm = Tools::getValue('comment');
        $rateForm = Tools::getValue('rate') !== false ? Tools::getValue('rate') : 0;

        if($comment) {

            $commentObject = new comment($comment['id_comment']);

            // Update
            if (Tools::isSubmit('comment_submit') &&
                ($commentObject->comment !== $commentForm || $commentObject->rate !== $rateForm)) {

                $commentObject->comment = $commentForm;
                $commentObject->rate = $rateForm;
                $commentObject->update();

            }

        } else {

            if(Tools::isSubmit('comment_submit')) {
                // Create
                $commentObject = new comment();
                $commentObject->id_customer = $idCustomer;
                $commentObject->comment = $commentForm;
                $commentObject->rate = $rateForm;
                $commentObject->active = Configuration::get('comment_default_active');
                $commentObject->add();
            }
        }
        
        $this->context->smarty->assign([
            'comment' => [
                'text' => isset($commentObject) ? $commentObject->comment : "",
                'rate' => isset($commentObject) ? $commentObject->rate : $rateForm,
            ]
        ]);

        $this->context->controller->addJS(_PS_MODULE_DIR_.'customercomment/views/js/createComment.js');
        $this->setTemplate('module:customercomment/views/templates/front/createComment.tpl');

    }
}