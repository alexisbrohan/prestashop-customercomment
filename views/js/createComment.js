(function ($) {

    $(document).ready(function () {

        $(".create_comment_form .material-icons").css('cursor', 'pointer');
        var inputRate = $(".create_comment_form input:hidden");
        var rate = inputRate.val();
        $(".create_comment_form .material-icons:lt(" + rate + ")").css('color', 'red');
        $(".create_comment_form .material-icons").hover(function () {

            $(".create_comment_form .material-icons").css('color', '');
            var index = $(this).data('index');
            $(".create_comment_form .material-icons:lt(" + index + ")").css('color', 'red');
            inputRate.attr('value', index);

        })
    });

})(jQuery);