(function ($) {

    $(document).ready(function () {

        var swiper = new Swiper('.swiper_comments', {
            speed: 400,
            slidesPerView: 1,
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
                dynamicBullets: true
            },
        });

        $(".rate").each(function () {
            var rate = $(this).data('rate');
            $(this).find(".material-icons:lt(" + rate + ")").text('star');
        });

    })
})(jQuery);

