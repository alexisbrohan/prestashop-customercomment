<h2 class="comments_title">Vos commentaires</h2>
<div class="comments">

    <div class="swiper-container swiper_comments">
        <div class="swiper-wrapper">
            {foreach $comments as $comment}
                <div class="swiper-slide">
                    <p><strong>{$comment['firstname']} {$comment['lastname']}</strong></p>
                    <p>{$comment['comment']}</p>
                    <div class="rate" data-rate="{$comment['rate']}">
                        {for $i=1; $i <= 5; $i++}
                            <i class="material-icons">star_border</i>
                        {/for}
                    </div>
                </div>
            {/foreach}
        </div>
        <div class="swiper-pagination"></div>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>

</div>
