<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="comment-link" href="{$link->getModuleLink('customercomment', 'createComment')}">
    <span class="link-item">
      <i class="material-icons">comment</i>
      {l s='Commentaires' mod='customercomment'}
    </span>
</a>