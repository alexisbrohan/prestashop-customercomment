{extends file="page.tpl"}
{block name="page_content"}

    <h1>{l s='Soumettez un commentaire' mod='customercomment'}</h1>
    <form method="post" action="" class="create_comment_form">
        <div class="form-group">
            <textarea class="form-control" name="comment" placeholder="Entrez votre commentaire" required>{$comment['text']}</textarea>
        </div>
        <div class="js_rate">
            {for $i=1; $i<=5; $i++}
                <i class="material-icons" data-index="{$i}">star</i>
            {/for}
        </div>
        <input type="hidden" name="rate" value="{$comment['rate']}">
        <input class="btn btn-primary float-xs-right hidden-xs-down" type="submit" name="comment_submit" value="Envoyer">
    </form>

{/block}